package com.epam.arsenal;

public enum WeaponType {
  BOW(CartridgeType.ARROW_HIGH),
  PISTOL(CartridgeType.PISTOL_BULLET_HIGH),
  ASSAULT_RIFLE(CartridgeType.ASSAULT_RIFLE_BULLET_HIGH);

  private final   Cartridge cartridge;

  WeaponType(CartridgeType cartridgeType) {
    this.cartridge = new Cartridge(cartridgeType);
  }

  public Cartridge getCartridge() {
    return cartridge;
  }
  //  !!!!!!!!!!!!!!!!!!!!!!!!!
}
