package com.epam.arsenal;

public enum CartridgeType {
  ARROW_LOW(2,2),
  ARROW_HIGH(10,10),

  PISTOL_BULLET_LOW(15,15),
  PISTOL_BULLET_HIGH(15,15),

  ASSAULT_RIFLE_BULLET_LOW(15,15),
  ASSAULT_RIFLE_BULLET_HIGH(15,15);

  private final int damage;
  private final int flyLength;

  CartridgeType(int damage, int flyLength) {
    this.damage = damage;
    this.flyLength = flyLength;
  }
}
