package com.epam.TerraBattle;


import java.util.Arrays;


public class BattleField {

  private static BattleField instance = null;
  private int[] ix;
  private int[] iy;

  private BattleField() {
    this.ix = new int[50];
    this.iy = new int[50];
    for (int i = 0; i < ix.length; i++) {
      ix[i] = i;
    }
    iy =ix;

  }

  public static BattleField getInstance() {
    if (instance == null) {
      instance = new BattleField();
    }
    return instance;
  }

  public int[] getIx() {
    return ix;
  }

  public int[] getIy() {
    return iy;
  }

  @Override
  public String toString() {
    return "BattleField{" +
        "ix=" + Arrays.toString(ix) +
        ", iy=" + Arrays.toString(iy) +
        '}';
  }
}