package com.epam.droids;

import com.epam.TerraBattle.BattleField;

public class Droid {

  private String name;
  private int hp;
  private int x;
  private int y;
  private DroidColorType droidColorType;
  private Weapon weapon;

//  public Droid(String name, int initX, DroidColorType droidColorType,
//      WeaponType weaponType) {
//    this.name = name;
//    this.hp = 100;
//    this.x = BattleField.getInstance().getIx()[initX];
//    this.y = BattleField.getInstance().getIy()[droidColorType.getInitY()];
//    this.droidColorType = droidColorType;
//    this.weapon = new Weapon(weaponType, );
//  }


  public Droid(String name, int initX, DroidColorType droidColorType,
      WeaponType weaponType) {
    this.name = name;
    this.hp = 100;
    this.x = BattleField.getInstance().getIx()[initX];
    this.y = BattleField.getInstance().getIy()[droidColorType.getInitY()];
    this.droidColorType = droidColorType;
    this.weapon = new Weapon(weaponType);
  }

  public void fight(Droid enemyDroid) {



  }


  public void setHp(int hp) {
    this.hp = hp;
  }


  public DroidColorType getDroidColorType() {
    return droidColorType;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  @Override
  public String toString() {
    return "Droid{" +
        "name='" + name + '\'' +
        ", hp=" + hp +
        ", x=" + x +
        ", y=" + y +
        ", droidColorType=" + droidColorType +
        ", weapon=" + weapon +
        '}';
  }
}
