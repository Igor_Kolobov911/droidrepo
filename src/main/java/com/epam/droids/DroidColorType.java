package com.epam.droids;

public enum DroidColorType {
  RED (0),
  BLUE (49);
  private final int initY;

  DroidColorType(int initY) {
    this.initY = initY;
  }

  public int getInitY() {
    return initY;
  }
}
